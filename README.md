# ssm

[![pipeline status](https://gitlab.science.ru.nl/mlubbers/ssm/badges/master/pipeline.svg)](https://gitlab.science.ru.nl/mlubbers/ssm/commits/master)

Download the latest version [here][0]

Simple Stack Machine interpreter

Forked from http://www.staff.science.uu.nl/~dijks106/SSM/ (dead link), https://github.com/atzedijkstra/ssm

### Documentation

For now the documentation is available only from within the program.

### Authors & Contributors

- Atze Dijkstra
- Mart Lubbers
- Thomas Churchman

[0]: https://gitlab.science.ru.nl/mlubbers/ssm/builds/artifacts/master/file/ssm.zip?job=build
